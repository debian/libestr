#!/bin/sh
# autopkgtest check: Build and run a program against libestr, to verify that the
# headers are correctly installed
# (C) 2012 Canonical Ltd.
# Author: Vibhav Pant <vibhavp@ubuntu.com>

set -e

if [ -n "${DEB_HOST_GNU_TYPE:-}" ]; then
    CROSS_COMPILE=${DEB_HOST_GNU_TYPE}-
else
    CROSS_COMPILE=
fi

WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM
cd $WORKDIR
cat <<EOF > estr_test.c
#include <libestr.h>
#include <string.h>
#include <stdio.h>

int main(void)
{

	es_str_t *estr;
	/* es_newStrFromCStr: Create a new string object based on a 
         * "traditional" C string. */
	estr = es_newStrFromCStr("autopkgtest", 15);

	if (estr ==  NULL) {
                fprintf(stderr, "es_newStrFromCStr failed");
		return 1;
        }
	/* es_strbufcmp: Compare a string against a buffer */
	if (es_strbufcmp(estr, "autopkgtest", 15) != 0) {
                fprintf(stderr, "es_strbufcmp failed");
                return 1;
        }
	/* es_str2cstr: Obtain a traditional C-String from a string object */
	if(es_str2cstr(estr, NULL) == NULL) {
                fprintf(stderr, "es_str2cstr failed");
		return 1;
        }
	/* es_extendBuf: Extend string buffer */
	if(es_extendBuf(&estr, 8 ) != 0) {
                fprintf(stderr, "es_extendBuf failed");
		return 1;
        }
	/* es_addChar: Append a character to the current string object */
	if(es_addChar(&estr, 's') != 0) {
                fprintf(stderr, "es_addChar failed");
		return 1;
        }
	/* es_addBuf: Append a memory buffer to a string */
	if(es_addBuf(&estr," Ubuntu", 7) != 0) {
                fprintf(stderr, "es_addBuf failed");
		return 1;
        }
	/* es_deleteStr: delete a string object. */
	es_deleteStr(estr);
	return 0;
}
EOF

${CROSS_COMPILE}gcc -o estr_test estr_test.c `pkg-config --cflags --libs libestr`
echo "build: OK"
[ -x estr_test ]
./estr_test
echo "run: OK" 
